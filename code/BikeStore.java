//Vinuyan Sivakolunthu 
//2037402

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bikes = new Bicycle[4];
        bikes[0] = new Bicycle("Wheelies", 10, 5.5);
        bikes[1] = new Bicycle("Big Wheel", 20, 8.45);
        bikes[2] = new Bicycle("Unicycle", 30, 3.59);
        bikes[3] = new Bicycle("Three Wheels", 5, 0.11);

        for (int i = 0; i < bikes.length; i++) {
            System.out.println(bikes[i]);
        }
    }
}
